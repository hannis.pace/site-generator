import Head from 'next/head'
import Link from 'next/link'
import styles from './layout.module.scss'
import Typography from 'typography'
import funstonTheme from 'typography-theme-funston'
import Darkmode from 'darkmode-js';

const typography = new Typography(funstonTheme)
typography.injectStyles()
new Darkmode().showWidget();

export default function Layout({ children }) {
	return (
		<>
			<Head>
				<meta name="viewport" content="initial-scale=1.0, width=device-width" />
			</Head>
			<div className={styles.container}>
				{children}
			</div>
		</>
	)
}

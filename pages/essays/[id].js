import Head from 'next/head'
import Link from 'next/link'
import Layout from '../../components/layout'
import Date from '../../components/date'
import styles from './essay.module.scss'
import { getAllEssayIds, getEssay } from '../../lib/content'

export async function getStaticProps({ params }) {
	return {
		props: {
			essay: await getEssay(params.id)
		}
	}
}

export async function getStaticPaths() {
	return {
		paths: await getAllEssayIds(),
		fallback: false
	}
}

export default function Essay({ essay }) {
	return (
		<Layout>
			<Head>
				<title>{essay.title}</title>
			</Head>
			<Link href='/'>
				<a>Back to essays</a>
			</Link>
			<article>
				<div className={styles.header}>
					<Date dateString={essay.date} />
					<p>{essay.min} min read</p>
				</div>
				<div dangerouslySetInnerHTML={{ __html: essay.contentHtml }} />
			</article>
			<Link href='/'>
				<a>Back to essays</a>
			</Link>
		</Layout>
	)
}


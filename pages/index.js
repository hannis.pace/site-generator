import Head from 'next/head'
import Link from 'next/link'
import Date from '../components/date'
import Layout from '../components/layout'
import { init, getLatestEssays } from '../lib/content'

export async function getStaticProps() {
	await init()
	const latestEssays = await getLatestEssays()
	return {
		props: {
			latestEssays
		}
	}
}

export default function Home({ latestEssays }) {
	return (
		<Layout>
			<Head>
				<title>My Blog</title>
			</Head>
			<section>
				<div>
					{latestEssays.map(({ id, date, min, title }) => (
						<div key={id}>
							<Link href="/essays/[id]" as={`/essays/${id}`}>
								<a>{title}</a>
							</Link>
							<br />
							<small>
								<Date dateString={date} />
								<p>{min} min read</p>
							</small>
						</div>
					))}
				</div>
			</section>
		</Layout>
	)
}

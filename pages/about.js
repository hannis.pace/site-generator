import { mdToHtml } from '../lib/content'
import path from 'path'
import Layout from '../components/layout'

export async function getStaticProps() {
	const readMePath = path.join(process.cwd(), 'README.md')
	return {
		props: {
			contentHtml: await mdToHtml(readMePath)
		}
	}
}

export default function About({ contentHtml }) {
	return (
		<Layout>
			<div dangerouslySetInnerHTML={{ __html: contentHtml }} />
		</Layout>
	)
}
